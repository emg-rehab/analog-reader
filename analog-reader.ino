static const int ANALOG_PIN = A3;

static const unsigned long BAUD_RATE = 1000000;
// Divide BAUD_RATE by 10 to get effective byte transmission rate.

static const unsigned long INTERVAL = 1000; // in microseconds

static unsigned long long nextTime = 0;

void setup() {
  Serial.begin(BAUD_RATE);

  while(!Serial);

  Serial.println();

  // Send a word for synchronization.
  Serial.println("START");

  // Send a JSON information header:
  // {"interval": <printing interval in microseconds>}
  // For more complex cases, the ArduinoJSON library can be used.
  Serial.print("{\"interval\":");
  Serial.print(INTERVAL);
  Serial.println("}");
  Serial.flush();
  nextTime = micros();
}

static unsigned long long mean = 0;
static unsigned long long counter = 0;

void loop() {
  nextTime += INTERVAL;
  mean = 0;
  counter = 0;
  do {
    mean += analogRead(ANALOG_PIN);
    counter++;
  }
  while (micros() < nextTime);

  mean /= counter;

  Serial.println(unsigned(mean));
}
